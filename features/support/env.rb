require 'faraday'
require 'json'
require 'rspec/expectations'
require 'rspec'
require 'rack/test'
require 'rspec/expectations'
require 'byebug'
require 'sinatra'
require_relative '../../app.rb'

RACK_ENV='test'
BASE_URL = 'http://localhost:4567'
if ENV['BASE_URL']
  BASE_URL = ENV['BASE_URL']
else
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end
