#language: es
Característica: Cartera de inversiones de persona fisica

  Antecedentes:
    Dado que mi cuit es "20112223336"

  Esquema del escenario: Inversion solo en dolares
    Dado que compro $ <capital> dólares a cotizacion $ <cotizacion_que_compre>
    Cuando vendo esos dolares a cotización $ <cotizacion_que_vendi>
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>

  Ejemplos:
    | capital | cotizacion_que_compre | cotizacion_que_vendi | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |    1000 |        80.5           |      90.0            |       9500.0   |       95.0           |    9405.0    |
    |    1000 |        100.0          |     100.0            |            0   |        0             |         0    |
    |    1000 |        100.0          |      90.0            |     -10000.0   |        0             |  -10000.0    |
    |   10000 |        100.0          |     200.0            |    1000000.0   |        40000.0       |  960000.0    |

  Esquema del escenario: Inversion solo en plazo fijo
    Dado que el interes de plazo fijo es <interes> % anual
    Cuando invierto $ <capital> en un plazo fijo a <plazo> dias
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>
  Ejemplos:
    | interes | capital | plazo | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |   10    |   1000  | 365   |     100.0      |         0.0          |     100.0    |
    |   10    |   1000  | 30    |      8.22      |         0.0          |      8.22    |
    

  Esquema del escenario: Inversion solo en acciones
    Dado que compro $ <capital> en acciones a $ <precio_por_accion_compra> por acción
    Cuando vendo esas acciones a $ <precio_por_accion_venta> por acción
    Entonces obtengo una ganancia bruta de $ <ganancia_bruta>
    Y $ <impuestos_comisiones> de impuestos-comisiones
    Y eso resulta en una ganancia neta de $ <ganancia_neta>

  Ejemplos:
    | capital | precio_por_accion_compra | precio_por_accion_venta | ganancia_bruta | impuestos_comisiones |ganancia_neta |
    |    1000 |        50.0              |     100.0               |       1000.0   |       10.0           |    990.0     |
    |    4000 |        10.0              |     100.0               |      36000.0   |     2160.0           |  33840.0     |
    |   10000 |        50.0              |     100.0               |      10000.0   |      300.0           |   9700.0     |
    |   10000 |        10.0              |     100.0               |      90000.0   |     5400.0           |  84600.0     |   
    |   10000 |        100.0             |      50.0               |      -5000.0   |       10.0           |  -5010.0     |
    |    4000 |        100.0             |      50.0               |      -2000.0   |       10.0           |  -2010.0     |